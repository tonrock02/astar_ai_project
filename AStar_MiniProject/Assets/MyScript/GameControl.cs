﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameControl : MonoBehaviour
{
    public Text PointsText;

    public int currentpoints;

    [SerializeField] private GameObject WinConUI;

    [SerializeField] private GameObject LoseConUI;
    public int Goalpoint;
    public bool ImWin;

    public bool ImLose;
    public static GameControl Instance { get; set; }
    
    // Start is called before the first frame update
    void Start()
    {
        if (Instance == null)
        {
            Instance = this;
        }

        Time.timeScale = 1;
    }

    // Update is called once per frame
    void Update()
    {
        PointsText.text = "Point : " + currentpoints.ToString();

        if (ImWin)
        {
            WinConUI.SetActive(true);
            Time.timeScale = 0;
        }

        if (ImLose)
        {
            LoseConUI.SetActive(true);
            Time.timeScale = 0;
        }

        if (currentpoints >= Goalpoint)
        {
            ImWin = true;
        }
    }

    public void UpdatePoints(int getpoints)
    {
        currentpoints += getpoints;
    }
    
    public void YouLose(bool check)
    {
        ImLose = check;
    }
}
