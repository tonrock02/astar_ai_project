﻿using System;
using System.Collections;
using System.Collections.Generic;
using Pathfinding;
using UnityEngine;

public class EnemySight : MonoBehaviour
{
    public GameObject EnemySightRange;
    public bool StayCheck=false;

    private float CooldownSeek;

    public float SeekTimer;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (CooldownSeek > 0)
        {
            CooldownSeek -= Time.deltaTime;
        }
        // else
        if(StayCheck==false&& CooldownSeek <=0)
        {
            EnemySightRange.GetComponent<AIPath>().enabled = false;
        }
        Debug.Log(CooldownSeek);
    }

    // void OnTriggerEnter2D(Collider2D other)
    // {
    //     if (other.CompareTag("Player"))
    //     {
    //         EnemySightRange.GetComponent<AIPath>().enabled = true;
    //         Debug.Log("Hit");
    //     }
    // }

    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            CooldownSeek = SeekTimer;
            EnemySightRange.GetComponent<AIPath>().enabled = true;
            StayCheck = true;
            Debug.Log("Stay");
            
        }
    }
    
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            
            StayCheck = false;
            Debug.Log("Leave");
        }
    }
}
